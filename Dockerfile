## Build Stage
FROM node AS build-stage
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY ./ /app/
RUN npm run build


## Final Image
FROM nginx:stable-alpine
RUN chown nginx:nginx /var/cache/nginx /var/run /etc/nginx/conf.d && chmod a+rwx /var/cache/nginx /var/run /etc/nginx/conf.d
USER nginx:nginx
COPY --from=build-stage /app/build/ /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
